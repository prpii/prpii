package com.pprog2.prpii;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private JSONArray chatArray;
    private Context context;


    // El Adaptador necesita siempre un ViewHolder.
    // El viewholder sirve para reciclar los items.
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Atributos comunes entre lo que reciclamos.
        private final TextView chatPerson;
        private final ImageView rectanguloChat;
        private final TextView ultimoMensaje;
        private JSONObject chatObject;

        // El ViewHolder requiere de una vista
        public ViewHolder(View view) {
            super(view);

            // Declaramos el findViewByID.
            itemView.setOnClickListener(this);
            chatPerson = (TextView) view.findViewById(R.id.DestinationChat);
            rectanguloChat = (ImageView) view.findViewById(R.id.rectanguloChat);
            ultimoMensaje = (TextView) view.findViewById(R.id.ultimoMensaje);
        }


        public void bind(JSONObject chat) { chatObject = chat; }

        public TextView getUltimoMensaje() {
            return ultimoMensaje;
        }

        public TextView getChatPerson() {
            return chatPerson;
        }

        public ImageView getRectanguloChat() {
            return rectanguloChat;
        }


        @Override
        public void onClick(View v) {

            // Entramos al chat y enviamos la información de con quién estamos hablando.
            Context context = v.getContext();
            Intent intent = new Intent(context, InsideChatActivity.class);

                try {
                    intent.putExtra("chatWith", chatObject.getString("id"));
                    intent.putExtra("NameDestination", chatObject.getString("name"));
                    intent.putExtra("LastNameDestination", chatObject.getString("last_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                context.startActivity(intent);
        }
    }

    // Constructor del adaptador de lista que recibe un JSONArray de lista de eventos.
    public ChatAdapter(JSONArray chatArray, Context contextList) {
        this.chatArray = chatArray;
        context = contextList;
    }

    public Context getContext() {
        return context;
    }

    // Aquí es donde creamos la vista, los items. Retornamos el ViewHolder.
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Le pasamos la vista para que la pueda llenar.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {

            // Obtenemos
            String nameDestination = (String) chatArray.getJSONObject(position).get("name");

            // Seteamos
            holder.getChatPerson().setText(nameDestination);

            // Color del chat
            cargaColorChat(holder,position);


            // Obtenemos último mensaje de la conversación.
            RequestQueue queue = Volley.newRequestQueue(getContext());
            String url = "http://puigmal.salle.url.edu/api/messages/"+String.valueOf(chatArray.getJSONObject(position).get("id"));

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray mensajesArray = new JSONArray(response);
                                JSONObject mensaje = mensajesArray.getJSONObject(mensajesArray.length()-1);
                                String ultimoMensaje = (String) mensaje.get("content");

                                // Seleccionamos quién ha escrito el último mensaje para la previsualización.
                                if(((int) mensaje.get("user_id_send")) == Integer.parseInt(Api.getUserEventId())) {
                                    ultimoMensaje = "Tú: " + ultimoMensaje;
                                } else {
                                    ultimoMensaje = nameDestination + ": " + ultimoMensaje;
                                }

                                holder.getUltimoMensaje().setText(ultimoMensaje);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("resposta", "Hi ha hagut un error:" + error);
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer" + " " + Api.getToken());
                    return headers;
                }
            };
            // Add the request to the RequestQueue.
            queue.add(stringRequest);





            holder.bind(chatArray.getJSONObject(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        if(chatArray != null) {
            return chatArray.length();
        } else {
            return 0;
        }
    }

    private void cargaColorChat(ViewHolder holder, int position) {

        if(position%2 == 0) {
            holder.getRectanguloChat().setImageResource(R.drawable.rectangulo_chat_claro);
        } else {
            holder.getRectanguloChat().setImageResource(R.drawable.rectangulo_chat);
        }
    }



}