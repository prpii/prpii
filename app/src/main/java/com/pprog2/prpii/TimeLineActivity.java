package com.pprog2.prpii;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeLineActivity extends AppCompatActivity {

    private JSONArray eventsArray;
    private RecyclerView mRecyclerView;
    private timeLineAdapter mAdapter;
    private BottomNavigationView barraNavegacion;
    String mtipoEvento = "Music";
    String mlistadoEvento = "Tus eventos futuros";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_timeline);

        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}


        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.chat:
                        Intent newIntent = new Intent(TimeLineActivity.this, ChatListActivity.class);
                        startActivity(newIntent);
                        break;

                    case R.id.profile:
                        Intent newIntent2 = new Intent(TimeLineActivity.this, ProfileActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(TimeLineActivity.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;


                    case R.id.create_event:
                        Intent newIntent4 = new Intent(TimeLineActivity.this, CreateEvent.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });



        makeRequest();
        mRecyclerView = (RecyclerView) findViewById(R.id.TLeventRecycle);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateUI();
    }

    private void updateUI() {
        if (mAdapter == null) {
            mAdapter = new timeLineAdapter(eventsArray,this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }


    /**
     * Método encargado de realizar la petición para obtener el listado de eventos según sus filtros.
     */
    private void makeRequest(){
        RequestQueue queue = Volley.newRequestQueue(this);

        System.out.println(Api.getUserEventId());
        System.out.println(Api.getToken());
        String url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/finished";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            eventsArray = new JSONArray(response);
                            ordenaCronologicamente();
                            System.out.println("DIMENSION: "+ eventsArray.length());

                            mAdapter = new timeLineAdapter(eventsArray,TimeLineActivity.this);
                            mRecyclerView.setAdapter(mAdapter);
                            System.out.println();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    /**
     * Método encargado de ordenar cronológicamente la lista de eventos del TimeLine.
     */
    private void ordenaCronologicamente() {


        // Lista auxiliar de objetos.
            List list = new ArrayList();
            for(int i = 0; i < eventsArray.length(); i++) {
                try {
                    list.add(eventsArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Preparamos método para ordenar cronológicamente según su fecha de inicio.
            Collections.sort(list, new Comparator() {
                private static final String KEY_NAME = "eventStart_date";
                @Override
                public int compare(Object o1, Object o2) {

                    JSONObject a = (JSONObject) o1;
                    JSONObject b = (JSONObject) o2;
                    String str1 = "";
                    String str2 = "";

                    // Obtenemos keys a comparar.
                    try {
                        str1 = (String) a.getString(KEY_NAME);
                        str2 = (String) b.getString(KEY_NAME);
                    } catch(JSONException e) {
                        e.printStackTrace();
                    }
                    return str1.compareTo(str2);
                }
            });

            JSONArray ordenado = new JSONArray();
            for(int i = 0; i < eventsArray.length(); i++) {
                ordenado.put(list.get(i));
            }
            eventsArray = ordenado;
        }
}
