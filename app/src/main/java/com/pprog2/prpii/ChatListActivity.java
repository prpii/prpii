package com.pprog2.prpii;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class ChatListActivity extends AppCompatActivity {


    private JSONArray chatsArray;
    private RecyclerView mRecyclerView;
    private ChatAdapter mAdapter;
    private BottomNavigationView barraNavegacion;
    private ImageButton botonBusqueda;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_list);

        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        // Botón de buscar
        botonBusqueda = (ImageButton) findViewById(R.id.buttonSearch);
        botonBusqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(ChatListActivity.this, BuscadorActivity.class);
                startActivity(newIntent);
            }
        });

        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.profile:
                        Intent newIntent = new Intent(ChatListActivity.this, ProfileActivity.class);
                        startActivity(newIntent);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(ChatListActivity.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(ChatListActivity.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.create_event:
                        Intent newIntent4 = new Intent(ChatListActivity.this, CreateEvent.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });



        // Petición de datos a mostrar por pantalla.
        makeRequest();
        mRecyclerView = (RecyclerView) findViewById(R.id.eventRecycleChat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateUI();

    }

    private void updateUI() {
        makeRequest();
        if (mAdapter == null) {
            mAdapter = new ChatAdapter(chatsArray,this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
            mAdapter.notifyItemRangeChanged(0,chatsArray.length());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateUI();
    }

    private void makeRequest(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/messages/users";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            chatsArray = new JSONArray(response);
                            System.out.println(chatsArray.toString());
                            mAdapter = new ChatAdapter(chatsArray,ChatListActivity.this);
                            mRecyclerView.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


}
