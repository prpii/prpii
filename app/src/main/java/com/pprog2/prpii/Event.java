package com.pprog2.prpii;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Event extends AppCompatActivity {

    // Atributos de un evento.
    private String eventTitle;
    private Image eventImage;
    private String eventLocation;
    private String eventDescription;
    private String eventCreation_date;
    private String eventStart_date;
    private String eventEnd_date;
    private String eventID;
    private int participators;
    private String type;
    private boolean estoyApuntado;
    private boolean heParticipado = false;
    private String owner_id;
    private String image;

    // Elementos de la vista a rellenar.
    private TextView eventTitleIn;
    private TextView eventDescriptionIn;
    private TextView eventLocationIn;
    private TextView eventCreationIn;
    private TextView eventStartIn;
    private TextView eventEndIn;
    private TextView eventMaxIn;
    private TextView eventTypeIn;
    private ImageButton eventChateaIn;
    private ImageView typeEventImage;
    private Button actionButtonEvent;
    private TextView eventChateaTextIn;
    private RatingBar ratingBarEvent;
    private TextView comentarioText;
    private EditText comentarioTextIn;
    private ImageButton butEditEvent;
    private BottomNavigationView barraNavegacion;
    private ImageButton edDeleteBut;

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_view);


        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        // Recogemos la información.
        eventTitle = getIntent().getStringExtra("eventTitle");
        eventLocation = getIntent().getStringExtra("eventLocation");
        eventDescription = getIntent().getStringExtra("eventDescription");
        eventCreation_date = getIntent().getStringExtra("eventCreation");
        eventStart_date = getIntent().getStringExtra("eventDate");
        eventEnd_date = getIntent().getStringExtra("eventEndDate");
        participators = getIntent().getIntExtra("eventParticipators",0);
        type = getIntent().getStringExtra("eventType");
        eventID = getIntent().getStringExtra("eventID");
        heParticipado = getIntent().getBooleanExtra("heParticipado",false);
        owner_id = getIntent().getStringExtra("owner_id");
        image = getIntent().getStringExtra("image");


        // Asignamos ID's a los componentes.
        eventTitleIn = (TextView) findViewById(R.id.eventTitleIn);
        eventDescriptionIn = (TextView) findViewById(R.id.eventDescriptionIn);
        eventLocationIn = (TextView) findViewById(R.id.eventLocationIn);
        eventCreationIn = (TextView) findViewById(R.id.eventCreationIn);
        eventStartIn = (TextView) findViewById(R.id.eventStartIn);
        eventEndIn = (TextView) findViewById(R.id.eventEndIn);
        eventMaxIn = (TextView) findViewById(R.id.eventMaxIn);
        eventTypeIn = (TextView) findViewById(R.id.eventTypeIn);
        typeEventImage = (ImageView) findViewById(R.id.typeEventImage);
        actionButtonEvent = (Button) findViewById(R.id.eventApuntameIn);
        eventChateaIn = (ImageButton) findViewById(R.id.eventChateaIn);
        eventChateaTextIn = (TextView) findViewById(R.id.preguntaCreadorTXT);
        ratingBarEvent = (RatingBar) findViewById(R.id.ratingBarEvent);
        comentarioText = (TextView) findViewById(R.id.comentarioText);
        comentarioTextIn = (EditText) findViewById(R.id.comentarioTextIn);
        butEditEvent = (ImageButton) findViewById((R.id.butEditEvent));
        edDeleteBut = (ImageButton) findViewById(R.id.edDeleteBut);

        // Asignamos textos a los componentes del evento.
        eventTitleIn.setText(eventTitle);
        eventLocationIn.setText(eventLocation);
        eventDescriptionIn.setText(eventDescription);

        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.bottomNavigationInEvent);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.chat:
                        Intent newIntent1 = new Intent(Event.this, ChatListActivity.class);
                        startActivity(newIntent1);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(Event.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(Event.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.create_event:
                        Intent newIntent5 = new Intent(Event.this, CreateEvent.class);
                        startActivity(newIntent5);
                        break;

                    case R.id.profile:
                        Intent newIntent4 = new Intent(Event.this, ProfileActivity.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });



        try {
            eventCreationIn.setText(extractFecha(eventCreation_date) + " a las " + extractHora(eventCreation_date));
            eventStartIn.setText(extractFecha(eventStart_date) + " a las " + extractHora(eventStart_date));
            eventEndIn.setText(extractFecha(eventEnd_date) + " a las " + extractHora(eventEnd_date));
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Hay algún error en el formato de fecha del evento.");
            eventCreationIn.setText("Error formato fecha.");
            eventStartIn.setText("Error formato fecha.");
            eventEndIn.setText("Error formato fecha.");
        }

        // Filtramos la posibilidad de apuntarse a un evento o de puntuarlo.

        if (isEventoFuturo() && isUserEvent()) {
            ratingBarEvent.setVisibility(View.INVISIBLE);
            eventChateaIn.setVisibility(View.INVISIBLE);
            eventChateaTextIn.setVisibility(View.INVISIBLE);
            comentarioText.setVisibility(View.INVISIBLE);
            comentarioTextIn.setVisibility(View.INVISIBLE);
            butEditEvent.setVisibility(View.VISIBLE);
            edDeleteBut.setVisibility(View.VISIBLE);
            getDataApuntado();

        } else if(isEventoFuturo()) {  // Se trata de un evento futuro por lo que nos podremos apuntar si lo deseamos.
            ratingBarEvent.setVisibility(View.INVISIBLE);
            eventChateaIn.setVisibility(View.VISIBLE);
            eventChateaTextIn.setVisibility(View.VISIBLE);
            comentarioText.setVisibility(View.INVISIBLE);
            comentarioTextIn.setVisibility(View.INVISIBLE);
            butEditEvent.setVisibility(View.INVISIBLE);
            edDeleteBut.setVisibility(View.INVISIBLE);
            getDataApuntado();                               // Actualizamos si estamos apuntados en el evento en cuestión.

        } else if (heParticipado) {     // Se trata de un evento pasado por lo que lo podremos puntuar en caso de haber participado.
            ratingBarEvent.setVisibility(View.VISIBLE);
            eventChateaIn.setVisibility(View.INVISIBLE);
            eventChateaTextIn.setVisibility(View.INVISIBLE);
            comentarioText.setVisibility(View.VISIBLE);
            comentarioTextIn.setVisibility(View.VISIBLE);
            actionButtonEvent.setText("¡Puntua!");
            butEditEvent.setVisibility(View.INVISIBLE);
            edDeleteBut.setVisibility(View.INVISIBLE);
            getPuntuacionEvento();
        } else {
            actionButtonEvent.setVisibility(View.INVISIBLE);
            ratingBarEvent.setVisibility(View.INVISIBLE);
            eventChateaIn.setVisibility(View.VISIBLE);
            eventChateaTextIn.setVisibility(View.VISIBLE);
            comentarioText.setVisibility(View.INVISIBLE);
            comentarioTextIn.setVisibility(View.INVISIBLE);
            butEditEvent.setVisibility(View.INVISIBLE);
            edDeleteBut.setVisibility(View.INVISIBLE);
        }

        actionButtonEvent.setOnClickListener(new View.OnClickListener() { // Añadimos listener para apuntarse al evento.
            @Override
            public void onClick(View v) {
                if(isEventoFuturo()) {
                    if(estoyApuntado){
                        actionButtonEvent.setText("¡APÚNTAME!");
                        desapuntarmeAPI();
                        estoyApuntado = false;
                    } else {
                        actionButtonEvent.setText("¡DESAPUNTARME! :(");
                        apuntarmeAPI();
                        estoyApuntado = true;
                    }
                } else {        // Se trata de un evento pasado por lo que lo podremos puntuar en caso de haber participado.
                    int rating = (int) ratingBarEvent.getRating();  // Rating del usuario
                    String comentario = comentarioTextIn.getText().toString();
                    puntuarEvento(rating,comentario);
                }
            }
        });

        // Acceder al chat con el creador.
        eventChateaIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Entramos al chat y enviamos la información de con quién estamos hablando.
                Context context = v.getContext();
                Intent intent = new Intent(context, InsideChatActivity.class);
                intent.putExtra("chatWith", owner_id);
                context.startActivity(intent);
            }
        });

        eventMaxIn.setText(Integer.toString(participators));
        eventTypeIn.setText(type);
        cargaImagen();

        butEditEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, EditEvent.class);

                intent.putExtra("eventTitle", eventTitle);
                intent.putExtra("eventDescription", eventDescription);
                intent.putExtra("eventLocation", eventLocation);
                intent.putExtra("eventStartDate", eventStart_date);
                intent.putExtra("eventEndDate", eventEnd_date);
                intent.putExtra("nParticipants", String.valueOf(participators));
                intent.putExtra("type", type);
                intent.putExtra("eventId", eventID);
                intent.putExtra("image", image);

                startActivity(intent);
            }
        });

        // Eliminar evento
        edDeleteBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRequestDeleteEvent(Event.this, eventID);
                Context context = v.getContext();
                Intent intent = new Intent(context, ListActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Método encargado de encargar una imágen u otra dependiendo del tipo de evento.
     */
    void cargaImagen() {

        switch(type) {

            case "Travel":
                typeEventImage.setImageResource(R.drawable.travelimg);
                break;

            case "Education":
                typeEventImage.setImageResource(R.drawable.educationimg);
                break;

            case "Sport":
                typeEventImage.setImageResource(R.drawable.sportimg);
                break;

            case "Music":
                typeEventImage.setImageResource(R.drawable.musicimg);
                break;

            case "Games":
                typeEventImage.setImageResource(R.drawable.gamesimg);
                break;

            case "Other":
                typeEventImage.setImageResource(R.drawable.otherimg);
                break;

            default:
                typeEventImage.setImageResource(R.drawable.otherimg);
                break;
        }
    }

    /**
     * Método encargado de extraer la fecha para mostrarla en la información del evento.
     * @param info Texto a parsear.
     * @return String correspondiente a la fecha.
     */
    private String extractFecha(String info) {
        String data[] = info.split("T");
        return data[0];
    }

    /**
     * Método encargado de extraer la hora para mostrarla en la información del evento.
     * @param info Texto a parsear.
     * @return String correspondiente a la hora.
     */
    private String extractHora(String info) {
        String data = info.substring(info.indexOf('T')+1, info.indexOf('T')+6);
        return data;
    }

    /**
     * Método encargado de realizar la petición para obtener el listado de eventos según sus filtros.
     */
    private void getDataApuntado(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/future";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray eventsArrayAssistance = new JSONArray(response);
                            // Miramos si el usuario está apuntado en el evento.
                            if(isApuntado(eventsArrayAssistance)){
                                actionButtonEvent.setText("¡DESAPUNTARME! :(");
                            } else {
                                actionButtonEvent.setText("¡APÚNTAME!");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    /**
     * Método encargado de mirar en la API si nos encontramos apuntado en el evento o no.
     * @param events Lista de eventos apuntados del usuario.
     * @return Booleano indicativo de si el usuario se encuentra apuntado o no.
     */
    private boolean isApuntado(JSONArray events) {

        for (int i = 0; i < events.length(); i++) {

            try {
                if(Integer.parseInt(eventID) == (Integer) events.getJSONObject(i).get("id")) {
                    estoyApuntado = true;
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        estoyApuntado = false;
        return false;
    }

    /**
     * Método encargado de puntuar el evento según los valores situados por el usuario.
     * @param rating    Valor entre 1 y 5.
     * @param comment   Comentario sobre la puntuación del usuario y el evento.
     */
    private void puntuarEvento(int rating, String comment) {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events/"+eventID+"/assistances";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("He actualizado la puntuación correctamente");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("puntuation", String.valueOf(rating));
                params.put("comentary", comment);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);
    }

    /**
     * Método encargado de actualizar por pantalla los datos de la puntuación anterior en caso de desear modificarlos.
     */
    private void getPuntuacionEvento() {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events/"+eventID+"/assistances/"+Api.getUserEventId();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("He accedido a la información del evento y del usuario correctamente.");

                        try {

                            JSONArray arr = new JSONArray(response);
                            JSONObject obj = (JSONObject) arr.getJSONObject(0);
                            int rating = obj.getInt("puntuation");
                            String comment = obj.getString("comentary");

                            // Asignamos valores para que los vea el usuario en caso de que haya puesto algunos.
                            if(rating >= 0 && rating <= 5 ) {
                                ratingBarEvent.setRating(rating);
                            }
                            if(!comment.equals(null)) {
                                comentarioTextIn.setText(comment);
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);


    }

    /**
     * Método de apuntar al usuario en el evento correspondiente dentro de la API.
     */
    private void apuntarmeAPI() {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events/"+eventID+"/assistances";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Me he apuntado correctamente al evento.");
                        System.out.println(Api.getUserEventId() + "KAKA");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("puntuation", "4");
                params.put("comentary", "muy bueno");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);
    }

    /**
     * Método de desapuntar al usuario en el evento correspondiente dentro de la API.
     */
    private void desapuntarmeAPI() {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events/"+eventID+"/assistances";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Me he desapuntado correctamente del evento.");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);
    }

    /**
     * Método encargado de mirar si se trata de un evento futuro o no para habilitar la opción de apuntarse o no.
     * @return Si se trata de un evento futuro a la fecha y hora del momento en el que se ha clickado.
     */
    private boolean isEventoFuturo() {
        Date todayDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date dateEvent = null;
        try {
            dateEvent = sdf.parse(eventStart_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //System.out.println(dateEvent.toString());

        if(todayDate.before(dateEvent)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Método encargado de mirar si el evento seleccionado se trata de un evento del usuario para poder modificar.
     * @return Si se trata de un evento del usuario.
     */
    private boolean isUserEvent() {
        String userIdApi = Api.getUserEventId();
        if (owner_id.equals(userIdApi)) {
            return true;
        } else {
            return false;
        }
    }

    public static void makeRequestDeleteEvent(Context context, String eventID) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/events/" + eventID;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public String getEventTitle() {
        return eventTitle;
    }
    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }
    public Image getEventImage() {
        return eventImage;
    }
    public void setEventImage(Image eventImage) {
        this.eventImage = eventImage;
    }
    public String getEventLocation() {
        return eventLocation;
    }
    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }
    public String getEventDescription() {
        return eventDescription;
    }
    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getParticipators() {
        return participators;
    }
    public void setParticipators(int participators) {
        this.participators = participators;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

}
