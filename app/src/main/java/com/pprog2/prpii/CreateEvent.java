package com.pprog2.prpii;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CreateEvent extends AppCompatActivity {

    private static final String CERO = "0";
    private static final String BARRA = "/";
    private static final String DOS_PUNTOS = ":";

    //Variables vista
    private EditText name;
    private EditText description;
    private EditText location;
    private Spinner spinner;
    private String type;
    private int numMax;
    private NumberPicker numberPicker;
    private Button crearEvento;
    private BottomNavigationView barraNavegacion;


    // Fecha inicio
    private EditText fechaPickerIni;
    private ImageButton botonFechaIni;
    // Hora inicio
    private EditText horaPickerIni;
    private ImageButton botonHoraIni;

    String eventStart_date;

    // Fecha Fin
    private EditText fechaPickerFin;
    private ImageButton botonFechaFin;
    // Hora Fin
    private EditText horaPickerFin;
    private ImageButton botonHoraFin;

    String eventEnd_date;

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    private final int mes = c.get(Calendar.MONTH);
    private final int dia = c.get(Calendar.DAY_OF_MONTH);
    private final int anio = c.get(Calendar.YEAR);

    //Variables para obtener la hora hora
    private final int hora = c.get(Calendar.HOUR_OF_DAY);
    private final int minuto = c.get(Calendar.MINUTE);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_evento_view);

        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}


        name = (EditText) findViewById(R.id.edName);
        description = (EditText) findViewById(R.id.edDescription);
        location = (EditText) findViewById(R.id.edLocation);


        // Fecha inicio
        fechaPickerIni = (EditText) findViewById(R.id.edFechaIni);
        botonFechaIni = (ImageButton) findViewById(R.id.edButFechaIni);
        botonFechaIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha(fechaPickerIni);
            }
        });

        // Hora inicio
        horaPickerIni = (EditText) findViewById(R.id.edHoraIni);
        botonHoraIni = (ImageButton) findViewById(R.id.edButHoraIni);
        botonHoraIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora(horaPickerIni);
            }
        });


        // Fecha Fin
        fechaPickerFin = (EditText) findViewById(R.id.edFechaFin);
        botonFechaFin = (ImageButton) findViewById(R.id.edButFechaFin);
        botonFechaFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha(fechaPickerFin);
            }
        });

        // Hora Fin
        horaPickerFin = (EditText) findViewById(R.id.edHoraFin);
        botonHoraFin = (ImageButton) findViewById(R.id.edButHoraFin);
        botonHoraFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora(horaPickerFin);
            }
        });


        numberPicker = findViewById(R.id.edNumberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(20000);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                int valuePicker = numberPicker.getValue();
                System.out.println(valuePicker);
            }
        });
        numMax = numberPicker.getValue();

        //Spinner
        spinner = (Spinner) findViewById(R.id.edType);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.event_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = parent.getItemAtPosition(position).toString();
                System.out.println(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        crearEvento = (Button) findViewById(R.id.edButEdEvento);
        crearEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api.searchUserId(CreateEvent.this, Api.getEmailAPI());
                eventStart_date = formatDateApi(fechaPickerIni.getText().toString(), horaPickerIni.getText().toString());
                eventEnd_date = formatDateApi(fechaPickerFin.getText().toString(), horaPickerFin.getText().toString());
                makeRequestCreateEvent(name, description, location, eventStart_date, eventEnd_date, type, numMax);
            }
        });

        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.chat:
                        Intent newIntent1 = new Intent(CreateEvent.this, ChatListActivity.class);
                        startActivity(newIntent1);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(CreateEvent.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(CreateEvent.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.profile:
                        Intent newIntent4 = new Intent(CreateEvent.this, ProfileActivity.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });


    };


    private void obtenerFecha(EditText picker){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                picker.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);

            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }

    private void obtenerHora(EditText picker){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                picker.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    private void makeRequestCreateEvent(EditText name, EditText description, EditText location, String eventStart_date, String eventEnd_date, String type, int NumMax) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String eventName = new JSONObject(response).getString("name");
                            System.out.println(eventName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name.getText().toString());
                params.put("image", "12345.jpg");
                params.put("location", location.getText().toString());
                params.put("description", description.getText().toString());
                params.put("eventStart_date", eventStart_date);
                params.put("eventEnd_date", eventEnd_date);
                params.put("n_participators", String.valueOf(NumMax));
                params.put("type", type);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);
    }



    /**
     * Función encargada de convertir la fecha introducida por el usuario a formato para poder ser guardada en la API.
     * @param fecha Fecha en formato dd/MM/yyyy
     * @param horas Hora en formato HH:MM pm ó HH:MM am
     * @return Retorna un string con la fecha en formato yyyy-MM-ddTHH:MM:00Z
     */
    public String formatDateApi(String fecha, String horas) {

        String str[] = fecha.split("/");
        String day = str[0];
        String month = str[1];
        String year = str[2];
        String str2[] = horas.split(":");
        String hour = str2[0];
        String minutes = str2[1].substring(0,2);


        String final_date = year+"-"+month+"-"+day+"T"+hour+":"+minutes+":00.000Z";
        System.out.println(final_date);
        return final_date;
    }


}
