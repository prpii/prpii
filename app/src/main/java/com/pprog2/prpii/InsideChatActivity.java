package com.pprog2.prpii;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class InsideChatActivity extends AppCompatActivity {


    private JSONArray messagesArray;
    private RecyclerView mRecyclerView;
    private InsideChatAdapter mAdapter;
    private String chatWith;
    private BottomNavigationView barraNavegacion;
    private Button enviaMSG;
    private EditText msgAEnviar;
    private TextView nombreDestinatario;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inside_chat_list);


        // Recogemos la información de con quién está hablando
        chatWith = getIntent().getStringExtra("chatWith");
        String nameChateando;
        nameChateando = getIntent().getStringExtra("NameDestination");
        nameChateando = nameChateando + " " + getIntent().getStringExtra("LastNameDestination");

        // Encontramos componentes
        enviaMSG = (Button) findViewById(R.id.enviarMSGButton);
        msgAEnviar = (EditText) findViewById(R.id.textoMSG);
        nombreDestinatario = (TextView) findViewById(R.id.DestinatarioName);

        // Seteamos valores a los componentes.
        nombreDestinatario.setText(nameChateando);

        // Seteamos acción de enviar mensaje una vez se ha presionado el botón.
        enviaMSG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = msgAEnviar.getText().toString();
                if(mensaje.length() > 0) {
                    requestEnviarMensaje(mensaje);
                    updateUI();
                }
            }
        });


        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.profile:
                        Intent newIntent = new Intent(InsideChatActivity.this, ProfileActivity.class);
                        startActivity(newIntent);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(InsideChatActivity.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(InsideChatActivity.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.create_event:
                        Intent newIntent4 = new Intent(InsideChatActivity.this, CreateEvent.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });


        // Petición de datos a mostrar por pantalla.
        mRecyclerView = (RecyclerView) findViewById(R.id.insideChatRecycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateUI();

    }

    private void updateUI() {
        makeRequest();
        if (mAdapter == null) {
            mAdapter = new InsideChatAdapter(messagesArray,this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
            mAdapter.notifyItemRangeChanged(0,messagesArray.length());
        }
    }

    private void makeRequest(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/messages/"+chatWith;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            messagesArray = new JSONArray(response);
                            System.out.println(messagesArray.toString());
                            mAdapter = new InsideChatAdapter(messagesArray,InsideChatActivity.this);
                            mRecyclerView.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void requestEnviarMensaje(String mensaje) {

        RequestQueue queue = Volley.newRequestQueue(this);
        System.out.println(Api.getUserEventId());
        String url = "http://puigmal.salle.url.edu/api/messages";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Se ha enviado el mensaje");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content", mensaje);
                params.put("user_id_send", String.valueOf(Api.getUserEventId()));
                params.put("user_id_recived", String.valueOf(chatWith));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);

    }

}
