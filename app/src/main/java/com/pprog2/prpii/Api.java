package com.pprog2.prpii;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Api {
    private static String token = null;
    private static String userEventId = null;
    private static String emailAPI = null;
    private static JSONArray eventosApuntados = null;

    public static JSONArray getEventosApuntados() {
        return eventosApuntados;
    }

    public static void setEventosApuntados(JSONArray eventosApuntados) {
        Api.eventosApuntados = eventosApuntados;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        Api.token = token;
    }

    public static String getUserEventId() {
        return userEventId;
    }

    public static void setUserEventId(String userEventId) {
        Api.userEventId = userEventId;
    }

    public static String getEmailAPI() {
        return emailAPI;
    }

    public static void setEmailAPI(String emailAPI) {
        Api.emailAPI = emailAPI;
    }

    public static void searchUserId(Context context, String email) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/users/search/?s=" + email;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONArray usuario =  new JSONArray(response);
                            JSONObject usuarioObj = (JSONObject) usuario.get(0);
                            userEventId = usuarioObj.getString("id");
                            emailAPI = usuarioObj.getString("last_name");
                            System.out.println("El id es: "+userEventId);
                            System.out.println("El email es: "+emailAPI);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("Error al obtener el id del usuario");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void makeRequestRegister(Context context, EditText name, EditText last_name, EditText email, EditText password) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/users";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            String tokenUser = new JSONObject(response).getString("email");
                            token = tokenUser;
                            System.out.println(token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("name", name.getText().toString());
                params.put("last_name", last_name.getText().toString());
                params.put("image", "123");
                params.put("email", email.getText().toString());
                params.put("password", password.getText().toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void makeRequestLogin(Context context, EditText email, EditText password, Activity activityOrigen) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/users/login";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            String tokenUser = new JSONObject(response).getString("accessToken");
                            token = tokenUser;
                            System.out.println(tokenUser);

                            Api.searchUserId(context, email.getText().toString());
                            Intent newIntent = new Intent(activityOrigen, ListActivity.class);
                            activityOrigen.startActivity(newIntent);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("email", email.getText().toString());
                params.put("password", password.getText().toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void makeRequestEvents(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/events";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("LALALAL");
                            JSONArray jsonArray = new JSONArray(response);
                            //String tokenUser = new JSONObject(response).getString("accessToken");
                            //token = tokenUser;
                            //System.out.println(tokenUser);
                            System.out.println();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                //params.put("email", email.getText().toString());
                //params.put("password", password.getText().toString());
                return params;

            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    // PRUEBA DE MAKEREQUEST
    public static void makeRequest(Context context){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://puigmal.salle.url.edu/api/events";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("LALALAL");
                            JSONArray eventsArray = new JSONArray(response);
                            //mAdapter = new eventListAdapter(eventsArray);
                            //mRecyclerView.setAdapter(mAdapter);
                            System.out.println();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        }
        );
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
