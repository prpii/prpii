package com.pprog2.prpii;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BuscadorAdapter extends RecyclerView.Adapter<BuscadorAdapter.ViewHolder> {

    private JSONArray usersArray;
    private Context context;


    // El Adaptador necesita siempre un ViewHolder.
    // El viewholder sirve para reciclar los items.
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Atributos comunes entre lo que reciclamos.
        private JSONObject chatObject;
        private ImageView rectanguloUser;
        private TextView nombreApellidoUser;

        // El ViewHolder requiere de una vista
        public ViewHolder(View view) {
            super(view);

            // Declaramos el findViewByID.
            itemView.setOnClickListener(this);

            // Declaramos el findViewByID.
            rectanguloUser = (ImageView) view.findViewById(R.id.rectanguloUser);
            nombreApellidoUser = (TextView) view.findViewById(R.id.nombreApellidoBuscador);

        }


        public void bind(JSONObject chat) { chatObject = chat; }

        public ImageView getRectanguloUser() {
            return rectanguloUser;
        }

        public TextView getNombreApellidoUser() {
            return nombreApellidoUser;
        }

        @Override
        public void onClick(View v) {

            // Entramos al chat y enviamos la información de con quién estamos hablando.
            Context context = v.getContext();
            Intent intent = new Intent(context, InsideChatActivity.class);

            try {
                intent.putExtra("chatWith", chatObject.getString("id"));
                intent.putExtra("NameDestination", chatObject.getString("name"));
                intent.putExtra("LastNameDestination", chatObject.getString("last_name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            context.startActivity(intent);
        }
    }

    // Constructor del adaptador de lista que recibe un JSONArray de lista de eventos.
    public BuscadorAdapter(JSONArray chatArray, Context contextList) {
        this.usersArray = chatArray;
        context = contextList;
    }

    public Context getContext() {
        return context;
    }

    // Aquí es donde creamos la vista, los items. Retornamos el ViewHolder.
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Le pasamos la vista para que la pueda llenar.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_buscador, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {

            // Seteamos
            String nombre = ((String) usersArray.getJSONObject(position).get("name"));
            nombre = nombre + " " + (String) usersArray.getJSONObject(position).get("last_name");
            holder.getNombreApellidoUser().setText(nombre);

            // Color del chat
            cargaColorChat(holder,position);

            holder.bind(usersArray.getJSONObject(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        if(usersArray != null) {
            return usersArray.length();
        } else {
            return 0;
        }
    }

    private void cargaColorChat(ViewHolder holder, int position) {

        if(position%2 == 0) {
            holder.getRectanguloUser().setImageResource(R.drawable.rectangulo_chat_claro);
        } else {
            holder.getRectanguloUser().setImageResource(R.drawable.rectangulo_chat);
        }
    }



}