package com.pprog2.prpii;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ListActivity extends AppCompatActivity {
    private JSONArray eventsArray;
    private RecyclerView mRecyclerView;
    private eventListAdapter mAdapter;
    private Spinner tipoEventos;
    private Spinner listadoEventos;
    private JSONArray eventosFiltrados;
    private BottomNavigationView barraNavegacion;
    String mtipoEvento = "Music";
    String mlistadoEvento = "Tus eventos futuros";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_list);

        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}


        // Spinner de filtrar por tipo de evento.
        tipoEventos = (Spinner) findViewById(R.id.spinnerEstado);
        ArrayAdapter<CharSequence> adapterTipo = ArrayAdapter.createFromResource(this, R.array.event_types, android.R.layout.simple_spinner_item);
        adapterTipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoEventos.setAdapter(adapterTipo);
        tipoEventos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mtipoEvento = parent.getItemAtPosition(position).toString();
                makeRequest();
                System.out.println(mtipoEvento);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Spinner de filtrar por tipo de evento.
        listadoEventos = (Spinner) findViewById(R.id.spinnerListado);
        ArrayAdapter<CharSequence> adapterListado = ArrayAdapter.createFromResource(this, R.array.event_list, android.R.layout.simple_spinner_item);
        adapterListado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listadoEventos.setAdapter(adapterListado);
        listadoEventos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mlistadoEvento = parent.getItemAtPosition(position).toString();
                makeRequest();
                System.out.println(mlistadoEvento);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.chat:
                        Intent newIntent = new Intent(ListActivity.this, ChatListActivity.class);
                        startActivity(newIntent);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(ListActivity.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.create_event:
                        Intent newIntent3 = new Intent(ListActivity.this, CreateEvent.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.profile:
                        Intent newIntent4 = new Intent(ListActivity.this, ProfileActivity.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });


        // Petición de datos a mostrar por pantalla.
        mRecyclerView = (RecyclerView) findViewById(R.id.eventRecycle);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateUI();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateUI();
    }

    private void updateUI() {
        makeRequest();
        if (mAdapter == null) {
            mAdapter = new eventListAdapter(eventosFiltrados,this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();

        }
    }


    /**
     * Método encargado de realizar la petición para obtener el listado de eventos según sus filtros.
     */
    private void makeRequest(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = urlEventos();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            eventsArray = new JSONArray(response);
                            System.out.println("DIMENSION: "+ eventsArray.length());
                            filtrarTipoEvento();
                            filtrarSiEsFuturo();
                            mAdapter = new eventListAdapter(eventosFiltrados,ListActivity.this);
                            mRecyclerView.setAdapter(mAdapter);
                            System.out.println();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    /**
     * Método encargado de obtener la URL de petición a la API según la opción seleccionada en el spinner.
     * @return URL de petición de listado de eventos para la API
     */
    private String urlEventos() {

        String url;

        switch(mlistadoEvento) {

            case "Tus eventos futuros":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/events/future";
                break;

            case "Tus eventos terminados":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/events/finished";
                break;

            case "Tus eventos en curso":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/events/current";
                break;

            case "Tus eventos":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/events";
                break;

            case "Eventos a los que te has apuntado":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/future";
                break;

            case "Eventos participados":
                url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/finished";
                System.out.println("Eventos participados show");
                break;

            case "All events":
                url = "http://puigmal.salle.url.edu/api/events";
                break;

            default:
                url = "http://puigmal.salle.url.edu/api/events";
                break;

        }
        return url;
    }

    /**
     * Método encargado de filtrar aquellos eventos futuros dentro de la lista de todos los eventos.
     */
    private void filtrarSiEsFuturo() {


        JSONArray eventosFuturos = new JSONArray();
        if(mlistadoEvento.equals("All Events")) {

            Date todayDate = new Date();

            for (int i = 0; i < eventosFiltrados.length(); i++) {
                try {
                    JSONObject obj = eventosFiltrados.getJSONObject(i);
                    try {

                        if(!eventosFiltrados.getJSONObject(i).get("eventStart_date").equals(null)) {
                            String sdateEvent = (String) eventosFiltrados.getJSONObject(i).get("eventStart_date");
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            Date dateEvent = sdf.parse(sdateEvent);
                            //System.out.println(dateEvent.toString());

                            if(todayDate.before(dateEvent)) {
                                System.out.println("------");
                                System.out.println(todayDate.toString());
                                System.out.println(dateEvent.toString());;
                                System.out.println(eventosFiltrados.getJSONObject(i).get("name"));
                                System.out.println("------");
                                eventosFuturos.put(obj);        // Añadimos evento futuro a la lista.
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            eventosFiltrados = eventosFuturos;      // Actualizamos listado de eventos filtrados anteriormente por tipo.
        }
    }

    /**
     * Método encargado de seleccionar que tipo de eventos queremos buscar en la API.
     */
    private void filtrarTipoEvento() {

        switch(mtipoEvento) {

            case "Travel":
                buscarEventos("Travel");
                break;

            case "Education":
                buscarEventos("Education");
                break;

            case "Sport":
                buscarEventos("Sport");
                break;

            case "Music":
                buscarEventos("Music");
                break;

            case "Games":
                buscarEventos("Games");
                break;

            case "Other":
                buscarEventos("Other");
                break;

            default:
                eventosFiltrados = eventsArray;
                break;

        }
    }

    /**
     * Método encargado de buscar el tipo de evento según el filtro de usuario: Music, Games...
     * @param typeEvent Tipo de evento seleccionado por el usuario.
     */
    private void buscarEventos(String typeEvent) {

        try {
            eventosFiltrados = new JSONArray();

            for (int i = 0; i < eventsArray.length(); ++i) {
                JSONObject obj = eventsArray.getJSONObject(i);
                String typeObj = obj.getString("type");
                if (typeObj.equals(typeEvent)) {
                    eventosFiltrados.put(eventsArray.get(i));
                }
            }
        } catch (JSONException e) {
            // handle exception
        }

    }


}
