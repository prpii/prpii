package com.pprog2.prpii;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InsideChatAdapter extends RecyclerView.Adapter<InsideChatAdapter.ViewHolder> {

    private static final int RECEIVED_VIEW = 1;
    private static final int SEND_VIEW = 2;
    private JSONArray messagesArray;
    private Context context;


    // El Adaptador necesita siempre un ViewHolder.
    // El viewholder sirve para reciclar los items.
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Atributos comunes entre lo que reciclamos.
        //private final TextView origenMSG;
        private final TextView origenMSG;
        private final TextView destinoMSG;
        private JSONObject chatObject;

        // El ViewHolder requiere de una vista
        public ViewHolder(View view) {
            super(view);

            // Declaramos el findViewByID.
            itemView.setOnClickListener(this);
            destinoMSG = (TextView) view.findViewById(R.id.destinoMSG);
            origenMSG = (TextView) view.findViewById(R.id.origenMSG);
        }

        public void bind(JSONObject chat) { chatObject = chat; }

        public TextView getOrigenMSG() {
            return origenMSG;
        }

        public TextView getDestinoMSG() {
            return destinoMSG;
        }

        @Override
        public void onClick(View v) {

        }
    }

    // Constructor del adaptador de lista que recibe un JSONArray de lista de eventos.
    public InsideChatAdapter(JSONArray chatArray, Context contextList) {
        this.messagesArray = chatArray;
        context = contextList;
    }

    public Context getContext() {
        return context;
    }

    // Aquí es donde creamos la vista, los items. Retornamos el ViewHolder.
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Le pasamos la vista para que la pueda llenar.
        switch(viewType) {

            case SEND_VIEW:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.in_message, parent, false);
                return new ViewHolder(view2);

            default:
                View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.out_message, parent, false);
                return new ViewHolder(view3);
        }
    }

    @Override
    public int getItemViewType(int position) {

        int user_id_send = 0;
        try {
            user_id_send = (int) messagesArray.getJSONObject(position).get("user_id_send");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Color del chat
        if(user_id_send == Integer.parseInt(Api.getUserEventId())) {
           return SEND_VIEW;

        } else {
            return RECEIVED_VIEW;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {

            // Obtenemos
            String msg = (String) messagesArray.getJSONObject(position).get("content");
            int user_id_send = (int) messagesArray.getJSONObject(position).get("user_id_send");

            // Seteamos


            // Miramos si somos nosotros quien ha escrito el mensaje para poner una burbuja u otra.
            if(user_id_send == Integer.parseInt(Api.getUserEventId())) {
                holder.getOrigenMSG().setText(msg);
            } else {
                holder.getDestinoMSG().setText(msg);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        if(messagesArray != null) {
            return messagesArray.length();
        } else {
            return 0;
        }
    }

}