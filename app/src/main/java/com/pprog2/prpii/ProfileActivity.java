package com.pprog2.prpii;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    private BottomNavigationView barraNavegacion;
    private TextView userNameTxtV;
    private EditText userEmail;
    private EditText userName;
    private EditText userLastName;
    private EditText password;
    private TextView eventosCreados;
    private TextView eventosParticipados;
    private TextView cantidadContactado;
    private Button buttonPruebaN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_view);

        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}


        // Recogemos información del usuario.
        userEmail = (EditText) findViewById(R.id.register_email);
        userName = (EditText) findViewById(R.id.register_name);
        userLastName = (EditText) findViewById(R.id.register_surname);
        password = (EditText) findViewById(R.id.register_password);
        buttonPruebaN = (Button) findViewById(R.id.buttonPruebaN);
        userNameTxtV = (TextView) findViewById(R.id.nombrePerfil);
        eventosCreados = (TextView) findViewById(R.id.eventosCreados);
        eventosParticipados = (TextView) findViewById(R.id.eventosParticipados);
        cantidadContactado = (TextView) findViewById(R.id.cantidadContactado);

        // Cargamos la información del usuario por pantalla.
        getUserInfo();

        // Cargamos información de estadísticas
        putNumEventosCreados();
        putNumEventosParticipados();
        putNumPersonasContactadas();

        buttonPruebaN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putUserInfo();
            }
        });


        // Barra de navegación inferior
        barraNavegacion = (BottomNavigationView) findViewById(R.id.BottomNavigationEvents);
        barraNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {

                    case R.id.chat:
                        Intent newIntent = new Intent(ProfileActivity.this, ChatListActivity.class);
                        startActivity(newIntent);
                        break;

                    case R.id.events:
                        Intent newIntent3 = new Intent(ProfileActivity.this, ListActivity.class);
                        startActivity(newIntent3);
                        break;

                    case R.id.timeline:
                        Intent newIntent2 = new Intent(ProfileActivity.this, TimeLineActivity.class);
                        startActivity(newIntent2);
                        break;

                    case R.id.create_event:
                        Intent newIntent4 = new Intent(ProfileActivity.this, CreateEvent.class);
                        startActivity(newIntent4);
                        break;

                }
                return false;
            }
        });

    }

    private void putNumEventosCreados(){

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/events";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray eventsArray = new JSONArray(response);
                            eventosCreados.setText(String.valueOf(eventsArray.length()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    private void putNumEventosParticipados(){

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/finished";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray eventsArray = new JSONArray(response);
                            eventosParticipados.setText(String.valueOf(eventsArray.length()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    private void putNumPersonasContactadas() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/messages/users";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray chatsArray = new JSONArray(response);
                            cantidadContactado.setText(String.valueOf(chatsArray.length()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    private void putUserInfo(){

        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://puigmal.salle.url.edu/api/users/";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Se ha actualizado la información correctamente");
                        Toast toast = Toast.makeText(getBaseContext(),"Se ha actualizado la información correctamente ",Toast.LENGTH_LONG);
                        toast.show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
                Toast toast = Toast.makeText(getBaseContext(),"Error en los datos introducidos",Toast.LENGTH_LONG);
                toast.show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("name", userName.getText().toString());
                params.put("last_name", userLastName.getText().toString());
                params.put("image", "123");
                params.put("email", userEmail.getText().toString());
                params.put("password", password.getText().toString());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    private void getUserInfo(){

        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId() ;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray arrInfo = new JSONArray(response);
                            System.out.println(arrInfo.toString());
                            JSONObject obj = arrInfo.getJSONObject(0);

                            userEmail.setText((String) obj.get("email"));
                            userName.setText((String) obj.get("name"));
                            userLastName.setText((String) obj.get("last_name"));
                            userNameTxtV.setText((String) obj.get("name"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resposta", "Hi ha hagut un error:" + error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}
