package com.pprog2.prpii;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {
    private EditText name;
    private EditText last_name;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private Button mRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        name = (EditText) findViewById(R.id.register_name);
        last_name = (EditText) findViewById(R.id.register_surname);
        email = (EditText) findViewById(R.id.register_email);
        password = (EditText) findViewById(R.id.register_password);
        confirmPassword = (EditText) findViewById(R.id.register_confirmpassword);
        mRegister = (Button) findViewById(R.id.signup_button);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Api.makeRequestRegister(RegisterActivity.this, name, last_name, email, password);
                checkRegisterData();
            }
        });
    }

    private void checkRegisterData() {

        String error = "";
        String prueba = name.getText().toString();

        if(name.getText().toString().length() == 0 || last_name.getText().toString().length() == 0 || email.getText().toString().length() == 0 || password.getText().toString().length() == 0) {
            error = error + ("- Hay algún campo vacío.\n");
        }

        String emailValidate = email.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (!emailValidate.matches(emailPattern))
        {
            error = error + ("- El formato de email es inválido.");
        }

        Toast toast = Toast.makeText(getBaseContext(),error,Toast.LENGTH_LONG);
        toast.show();
    }
}
