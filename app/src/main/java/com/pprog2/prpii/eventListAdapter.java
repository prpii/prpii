package com.pprog2.prpii;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class eventListAdapter extends RecyclerView.Adapter<eventListAdapter.ViewHolder> {

    private JSONArray events;
    private Context context;


    // El Adaptador necesita siempre un ViewHolder.
    // El viewholder sirve para reciclar los items.
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Atributos comunes entre lo que reciclamos.
        private final TextView eventTitle;
        private final TextView eventDescription;
        private final TextView eventLocation;
        private final TextView eventDate;
        private final ImageView eventImage;
        private final ImageView typeImage;
        private final ImageView imageItemEvent;
        private final ImageView fondoEvent;
        private JSONObject eventObject;
        private boolean isParticipado = false;

        // El ViewHolder requiere de una vista
        public ViewHolder(View view) {
            super(view);

            // Declaramos el findViewByID.
            itemView.setOnClickListener(this);
            eventTitle = (TextView) view.findViewById(R.id.eventTitle);
            eventDescription = (TextView) view.findViewById(R.id.eventDescription);
            eventLocation = (TextView) view.findViewById(R.id.eventLocation);
            eventDate = (TextView) view.findViewById(R.id.eventDate4);
            eventImage = (ImageView) view.findViewById(R.id.BloqueFondoEvent);
            typeImage = (ImageView) view.findViewById(R.id.eventType);
            imageItemEvent = (ImageView) view.findViewById(R.id.eventImageType);
            fondoEvent = (ImageView) view.findViewById(R.id.BloqueFondoEvent);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent intent = new Intent(context, Event.class);
            try {
                intent.putExtra("eventTitle", eventObject.getString("name"));
                intent.putExtra("eventDescription", eventObject.getString("description"));
                intent.putExtra("eventLocation", eventObject.getString("location"));
                intent.putExtra("eventDate", eventObject.getString("eventStart_date"));
                intent.putExtra("eventEndDate", eventObject.getString("eventEnd_date"));
                intent.putExtra("eventParticipators", eventObject.getInt("n_participators"));
                intent.putExtra("eventType", eventObject.getString("type"));
                intent.putExtra("eventCreation", eventObject.getString("date"));
                intent.putExtra("eventID", eventObject.getString("id"));
                intent.putExtra("owner_id", eventObject.getString("owner_id"));
                intent.putExtra("image", eventObject.getString("image"));

                    // Mirar si ha participado para actualizarlo y enviarlo al intent.
                    RequestQueue queue = Volley.newRequestQueue(context);
                    String url = "http://puigmal.salle.url.edu/api/users/"+Api.getUserEventId()+"/assistances/finished";
                    isParticipado = false;

                    // Request a string response from the provided URL.
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONArray eventsArray = new JSONArray(response);

                                        for (int i = 0; i < eventsArray.length(); i++) {

                                            if(eventsArray.getJSONObject(i).get("id").toString().equals(eventObject.getString("id"))) {
                                                isParticipado = true;
                                                System.out.println("VALOR ES: " + isParticipado);

                                                // Enviamos isParticipado al intent.
                                                intent.putExtra("heParticipado", isParticipado);
                                            }
                                        }
                                        context.startActivity(intent);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("resposta", "Hi ha hagut un error:" + error);
                        }
                    })

                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Authorization", "Bearer" + " " + Api.getToken());
                            return headers;
                        }
                    };
                    // Add the request to the RequestQueue.
                    queue.add(stringRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void bind(JSONObject event) { eventObject = event; }

        public TextView getEventTitle() {
            return eventTitle;
        }

        public TextView getEventDescription() {
            return eventDescription;
        }

        public TextView getEventLocation() {
            return eventLocation;
        }

        public TextView getEventDate() {
            return eventDate;
        }

        public ImageView getEventImage() {
            return eventImage;
        }

        public ImageView getTypeImage() {
            return typeImage;
        }

        public ImageView getImageItemEvent() {
            return imageItemEvent;
        }

        public ImageView getFondoEvent() {
            return fondoEvent;
        }

    }

    // Constructor del adaptador de lista que recibe un JSONArray de lista de eventos.
    public eventListAdapter(JSONArray eventsArray, Context contextList) {
        events = eventsArray;
        context = contextList;
    }

    // Aquí es donde creamos la vista, los items. Retornamos el ViewHolder.
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Le pasamos la vista para que la pueda llenar.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_event, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {

            // Obtenemos los datos del evento.
            String title = (String) events.getJSONObject(position).get("name");
            String description = (String) events.getJSONObject(position).get("description");
            String location = (String) events.getJSONObject(position).get("location");
            String type = (String) events.getJSONObject(position).get("type");
            int eventID = (Integer) events.getJSONObject(position).get("id");

            // Seteamos los valores al holder.
            holder.getEventTitle().setText(title);
            holder.getEventDescription().setText(description);
            holder.getEventLocation().setText(location);

            // Comprobamos fecha válida.
            if(!events.getJSONObject(position).get("eventStart_date").equals(null)) {
                String dateJSON = (String) events.getJSONObject(position).get("eventStart_date");
                String date[] = dateJSON.split("T");
                holder.getEventDate().setText(date[0]);
            }

            //String imgURL = (String) events.getJSONObject(position).get("image");
            //System.out.println("prueba: "+imgURL);
            //Picasso.get().load("https://i.imgflip.com/3lmzyx.jpg").into(holder.getEventImage());
            cargaIcono(holder,type);

            // Bindeamos al JSONArrayList de eventos.
            holder.bind(events.getJSONObject(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void cargaIcono(@NonNull ViewHolder holder, String type) {

        switch(type) {

            case "Travel":
                holder.getTypeImage().setImageResource(R.drawable.ic_travel_foreground);
                holder.getImageItemEvent().setImageResource(R.drawable.travelimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_verde);

                break;

            case "Education":
                holder.getTypeImage().setImageResource(R.drawable.ic_education_foreground);
                holder.getImageItemEvent().setImageResource(R.drawable.educationimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_rosa);
                break;

            case "Sport":
                holder.getTypeImage().setImageResource(R.drawable.ic_sport_foreground);
                holder.getImageItemEvent().setImageResource(R.drawable.sportimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_azul);
                break;

            case "Music":
                holder.getTypeImage().setImageResource(R.drawable.ic_music_foreground);
                holder.getImageItemEvent().setImageResource(R.drawable.musicimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_naranja);

                break;

            case "Games":
                holder.getTypeImage().setImageResource(R.drawable.ic_game_foreground);
                holder.getImageItemEvent().setImageResource(R.drawable.gamesimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_morado);
                break;

            case "Other":
                holder.getTypeImage().setImageResource(R.drawable.ic_other);
                holder.getImageItemEvent().setImageResource(R.drawable.otherimg);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_turquesa);
                break;

            default:
                holder.getTypeImage().setImageResource(R.drawable.ic_sobre_foreground);
                holder.getFondoEvent().setImageResource(R.drawable.evento_forma_turquesa);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if(events != null) {
            return events.length();
        } else {
            return 0;
        }
    }

    /*

    private void checkApuntado(JSONArray events, int eventID, ViewHolder holder) {

        System.out.println("Longitud: " + events.length() + events.toString());
        for (int i = 0; i < events.length(); i++) {
            try {
                System.out.println("EVENTITOS "+ events.getJSONObject(i).get("name"));

                if(eventID == (Integer) events.getJSONObject(i).get("id")) {
                    holder.getTickAsistencia().setImageResource(R.drawable.ic_tick_icon_foreground);
                } else {
                    holder.getTickAsistencia().setImageResource(android.R.color.transparent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
*/

}
