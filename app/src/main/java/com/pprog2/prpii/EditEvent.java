package com.pprog2.prpii;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class EditEvent extends AppCompatActivity {

    private static final String CERO = "0";
    private static final String BARRA = "/";
    private static final String DOS_PUNTOS = ":";

    //Variables de un evento
    private String eventName;
    private String eventDescription;
    private String eventLocation;
    private String eventStart_date;
    private String eventEnd_date;
    private String participators;
    private String type;
    private String eventId;
    private String image;


    //Elementos de la vista para rellenar y modificar
    private EditText edName;
    private EditText edDescription;
    private EditText edLocation;
    private EditText edImage;
    private EditText edFechaIni;
    private EditText edHoraIni;
    private ImageButton edButFechaIni;
    private ImageButton edButHoraIni;
    private EditText edFechaFin;
    private EditText edHoraFin;
    private ImageButton edButFechaFin;
    private ImageButton edButHoraFin;
    private NumberPicker edNumberPicker;
    private Spinner edType;
    private Button edButEdEvento;

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    private final int mes = c.get(Calendar.MONTH);
    private final int dia = c.get(Calendar.DAY_OF_MONTH);
    private final int anio = c.get(Calendar.YEAR);

    //Variables para obtener la hora hora
    private final int hora = c.get(Calendar.HOUR_OF_DAY);
    private final int minuto = c.get(Calendar.MINUTE);

    private String eventStart_dateForm;
    private String eventEnd_dateForm;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_evento_view);

        // Quitar barra superior de app.
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        //Recogemos informacion
        eventName = getIntent().getStringExtra("eventTitle");
        eventDescription = getIntent().getStringExtra("eventDescription");
        eventLocation = getIntent().getStringExtra("eventLocation");
        eventStart_date = getIntent().getStringExtra("eventStartDate");
        eventEnd_date = getIntent().getStringExtra("eventEndDate");
        participators = getIntent().getStringExtra("nParticipants");
        type = getIntent().getStringExtra("type");
        eventId = getIntent().getStringExtra("eventId");
        image = getIntent().getStringExtra("image");

        edName = (EditText) findViewById(R.id.edName);
        edDescription = (EditText) findViewById(R.id.edDescription);
        edLocation = (EditText) findViewById(R.id.edLocation);
        edImage = (EditText) findViewById(R.id.edImage);
        edFechaIni = (EditText) findViewById(R.id.edFechaIni);
        edButFechaIni = (ImageButton) findViewById(R.id.edButFechaIni);
        edHoraIni = (EditText) findViewById(R.id.edHoraIni);
        edButHoraIni = (ImageButton) findViewById(R.id.edButHoraIni);
        edFechaFin = (EditText) findViewById(R.id.edFechaFin);
        edButFechaFin = (ImageButton) findViewById(R.id.edButFechaFin);
        edHoraFin = (EditText) findViewById(R.id.edHoraFin);
        edButHoraFin = (ImageButton) findViewById(R.id.edButHoraFin);
        edNumberPicker = (NumberPicker) findViewById(R.id.edNumberPicker);
        edType = (Spinner) findViewById(R.id.edType);
        edButEdEvento = (Button) findViewById(R.id.edButEdEvento);

        //Asignamos texto
        edName.setText(eventName);
        edDescription.setText(eventDescription);
        edLocation.setText(eventLocation);
        edImage.setText(image);
        edFechaIni.setText(extractFecha(eventStart_date));
        edHoraIni.setText(extractHora(eventStart_date));
        edFechaFin.setText(extractFecha(eventEnd_date));
        edHoraFin.setText(extractHora(eventEnd_date));

        //Modificar fecha inicial
        edButFechaIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha(edFechaIni);
            }
        });

        //Modificar hora inicial
        edButHoraIni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora(edHoraIni);
            }
        });


        eventStart_dateForm = formatDateApi(edFechaIni.getText().toString(), edHoraIni.getText().toString());

        //Modificar fecha final
        edButFechaFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha(edFechaFin);
            }
        });

        //Modificar hora final
        edButHoraFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora(edHoraFin);
            }
        });

        eventEnd_dateForm = formatDateApi(edFechaFin.getText().toString(), edHoraFin.getText().toString());

        //Number Picker
        edNumberPicker.setMinValue(0);
        edNumberPicker.setMaxValue(20000);
        edNumberPicker.setValue(Integer.parseInt(participators));
        participators = String.valueOf(edNumberPicker.getValue());

        //Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.event_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edType.setAdapter(adapter);

        edType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edButEdEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRequestEditEvent(edName, edDescription, edLocation, eventStart_dateForm, eventEnd_dateForm, participators, type, eventId);
            }
        });

    }

    /**
     * Método encargado de extraer la fecha para mostrarla en la información del evento.
     * @param info Texto a parsear.
     * @return String correspondiente a la fecha.
     */
    private String extractFecha(String info) {
        String data[] = info.split("T");
        return data[0];
    }

    /**
     * Método encargado de extraer la hora para mostrarla en la información del evento.
     * @param info Texto a parsear.
     * @return String correspondiente a la hora.
     */
    private String extractHora(String info) {
        String data = info.substring(info.indexOf('T')+1, info.indexOf('T')+6);
        return data;
    }

    private void obtenerFecha(EditText picker){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                picker.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);

            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }


    private void obtenerHora(EditText picker){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                picker.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    /**
     * Función encargada de convertir la fecha introducida por el usuario a formato para poder ser guardada en la API.
     * @param fecha Fecha en formato dd/MM/yyyy
     * @param horas Hora en formato HH:MM pm ó HH:MM am
     * @return Retorna un string con la fecha en formato yyyy-MM-ddTHH:MM:00Z
     */
    public String formatDateApi(String fecha, String horas) {

        String str[] = fecha.split("-");
        String day = str[0];
        String month = str[1];
        String year = str[2];
        String str2[] = horas.split(":");
        String hour = str2[0];
        String minutes = str2[1].substring(0,2);


        String final_date = year+"-"+month+"-"+day+"T"+hour+":"+minutes+":00.000Z";
        System.out.println(final_date);
        return final_date;
    }

    private void makeRequestEditEvent(EditText edName, EditText edDescription, EditText edLocation, String eventStart_dateForm, String eventEnd_dateForm, String participators, String type, String eventId){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://puigmal.salle.url.edu/api/events/" + eventId;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String eventName = new JSONObject(response).getString("name");
                            System.out.println(eventName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", edName.getText().toString());
                params.put("image", "12345.jpg");
                params.put("location", edLocation.getText().toString());
                params.put("description", "Esto es una prueba para ver si funcionas jejejejejejjejejejeje");
                params.put("eventStart_date", eventStart_dateForm);
                params.put("eventEnd_date", eventEnd_dateForm);
                params.put("n_participators", participators);
                params.put("type", type);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + Api.getToken());
                return headers;
            }
        };

        queue.add(stringRequest);
    }
}
